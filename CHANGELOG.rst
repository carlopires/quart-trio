0.2.0 2019-01-29
----------------

* Use the latest Hypercorn and Quart run definition.
* Allow for background tasks to be started, the app now has a nursery
  for background tasks.
* Support Quart >= 0.8.0.
* Support serving static files.

0.1.0 2018-12-17
----------------

* Released initial alpha version.
